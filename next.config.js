/** @type {import('next').NextConfig} */
const nextConfig = {
  reactStrictMode: true,
  images: {
    domains: ['backend.codematica.it'],
  },
}

module.exports = nextConfig
