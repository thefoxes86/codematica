const { gql } = require('@apollo/client')

export const PAGE_SINGLE_QUERY = gql`
  query Single(
    $id: ID!
    $language: LanguageCodeEnum!
    $lang: LanguageCodeFilterEnum!
  ) {
    servizio(id: $id, idType: DATABASE_ID) {
      content(format: RENDERED)
      slug
      title
      translation(language: $language) {
        id
        slug
        content
        title
        language {
          locale
          slug
        }
      }
      featuredImage {
        node {
          sourceUrl(size: LARGE)
        }
      }
      serviziacf {
        numero
      }
    }
    servizi(first: 12, where: { language: $lang }) {
      edges {
        node {
          content(format: RENDERED)
          title(format: RENDERED)
          slug
          databaseId
          language {
            code
            locale
          }
          serviziacf {
            numero
          }
          featuredImage {
            node {
              sourceUrl(size: LARGE)
            }
          }
        }
      }
    }
  }
`

export const GET_NEWS = gql`
  query GET_NEWS {
    posts {
      nodes {
        slug
        date
        title(format: RENDERED)
        featuredImage {
          node {
            sourceUrl(size: MEDIUM)
          }
        }
      }
    }
  }
`

export const GET_NEWS_DETAIL = gql`
  query GET_NEWS_DETAIL($id: ID!) {
    post(id: $id, idType: SLUG) {
      featuredImage {
        node {
          sourceUrl(size: LARGE)
          title
        }
      }
      date
      title
      content(format: RENDERED)
    }
  }
`

export const GET_PAGE = gql`
  query GET_PAGE($id: ID!) {
    page(id: $id, idType: DATABASE_ID) {
      contenuti {
        testo1
        testo2
        titolo1
        titolo2
        titolo3
        titolo4
        immagine1 {
          node {
            sourceUrl(size: LARGE)
          }
        }
        immagine2 {
          node {
            sourceUrl(size: LARGE)
          }
        }
        mostraCampo1
        mostraCampo2
      }
    }
  }
`

export const GET_HOME = gql`
  query HOME($language: LanguageCodeFilterEnum!) {
    servizi(first: 12, where: { language: $language }) {
      edges {
        node {
          content(format: RENDERED)
          title(format: RENDERED)
          slug
          databaseId
          language {
            code
            locale
          }
          featuredImage {
            node {
              sourceUrl(size: LARGE)
            }
          }
          serviziacf {
            numero
          }
        }
      }
    }
    brands {
      edges {
        node {
          featuredImage {
            node {
              sourceUrl(size: LARGE)
            }
          }
          title(format: RENDERED)
        }
      }
    }
  }
`

export const GET_SINGLE_SERVIZIO = gql`
  query SERVIZO($id: ID!, $language: LanguageCodeEnum!) {
    servizio(id: $id, idType: SLUG) {
      title
      content(format: RENDERED)
      translation(language: $language) {
        id
        slug
        content
        title
        language {
          locale
          slug
        }
      }
      serviziacf {
        numero
      }
      featuredImage {
        node {
          sourceUrl(size: LARGE)
        }
      }
    }
  }
`
