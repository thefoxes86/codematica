import { Html } from '@react-three/drei'
import { motion } from 'framer-motion'
import AnimateSection from './AnimateSection'
import { services } from '@/utils/mock'
import Button from './Button'
import Image from 'next/image'
import { useFrame } from '@react-three/fiber'
import { useState } from 'react'
import { useScroll } from '@react-three/drei'
import { useMediaQuery } from '../hooks/useMediaQuery'
import Link from 'next/link'

const Services = ({
  topMd,
  topLg,
  topXl,
  position,
  pages,
  svg,
  servizi,
  lang,
}) => {
  const data = useScroll()
  const [topAbs, setTopAbs] = useState(0)
  const isMd = useMediaQuery('(max-width: 768px)')
  const isLg = useMediaQuery('(max-width: 1024px)')

  useFrame(() => {
    setTopAbs(data.range(position / pages, position / pages))
  })
  return (
    <div
      className="services"
      style={{ top: isMd ? topMd : isLg ? topLg : topXl }}
    >
      <AnimateSection
        initial={{ opacity: 0, y: 20 }}
        whileInView={{ opacity: 1, y: 0 }}
        transition={{ duration: 0.9, delay: 0 }}
        viewport={{ amount: 0.3 }}
        className="services__title"
      >
        <h3>Codematica è partner di costruttori di macchina ed impianti</h3>
        <h3 className="font-bold">Vicina al cliente dinale con dedizione</h3>
      </AnimateSection>
      <motion.div className="services__list">
        {servizi?.edges.map(({ node }, index) => (
          <AnimateSection
            key={index}
            delay={index * 0.2}
            className="services__item"
          >
            <Link href={`/${lang}/servizi/${node?.slug}`}>
              <motion.span>
                {index + 1 < 10 ? `0${index + 1}` : index + 1}
              </motion.span>
              <motion.p
                dangerouslySetInnerHTML={{ __html: node.title }}
                className="services__item__content"
              ></motion.p>
            </Link>
          </AnimateSection>
        ))}
      </motion.div>
      {svg && (
        <Image
          src={svg}
          alt=""
          style={{ transform: `translateY(-${topAbs * 450}px)` }}
        />
      )}
    </div>
  )
}

export default Services
