import Link from 'next/link'
import Button from './Button'
import Image from 'next/image'
import bandobanner from '../../public/images/bandobanner.png'
import leatherUp from '../../public/images/LEatherUP.png'
import phone from '../../public/images/phone.png'
import mail from '../../public/images/mail.png'
import linkedin from '../../public/images/linkedin.png'
import twitter from '../../public/images/twitter.png'
import { useMediaQuery } from '../hooks/useMediaQuery'
import AnimateSection from './AnimateSection'

const Footer = ({ topMd, topLg, topXl, absolute = true }) => {
  const isMd = useMediaQuery('(max-width: 768px)')
  const isLg = useMediaQuery('(max-width: 1024px)')
  return (
    <footer
      className={`${
        absolute ? 'absolute' : '!relative overflow-y-hidden !h-auto'
      }`}
      style={{ top: isMd ? topMd : isLg ? topLg : topXl }}
    >
      <div className={`${!absolute && '!h-auto py-10'} footer__black`}>
        <AnimateSection
          initial={{ opacity: 0, y: 20 }}
          whileInView={{ opacity: 1, y: 0 }}
          transition={{ duration: 0.9, delay: 0 }}
          viewport={{ amount: 0.3 }}
        >
          <div>
            <p className="mb-10">
              Codematica è sempre alla ricerca di personale tecnico da inserire
              in azienda.
            </p>
            <Button>
              <Link href={'/lavora-con-noi'}>CANDIDATI</Link>
            </Button>
          </div>
        </AnimateSection>
        <AnimateSection
          initial={{ opacity: 0, y: 20 }}
          whileInView={{ opacity: 1, y: 0 }}
          transition={{ duration: 0.9, delay: 0.2 }}
          viewport={{ amount: 0.3 }}
        >
          <div>
            <p>
              Codematica Srl<br></br> Via della Pavoncella 29<br></br> Vecchiano
              (Pisa), Italy 56019
            </p>
            <p>
              Tel (+39) 050 804199<br></br> Fax (+39) 050 38351152<br></br>{' '}
              eMail info@codematica.it
            </p>
          </div>
        </AnimateSection>
        <AnimateSection
          initial={{ opacity: 0, y: 20 }}
          whileInView={{ opacity: 1, y: 0 }}
          transition={{ duration: 0.9, delay: 0.4 }}
          viewport={{ amount: 0.3 }}
        >
          <div className="social">
            <div>
              <a href="/">
                <Image src={phone} alt="tel" width={25} height={25} />
              </a>
              <a href="/">
                <Image src={linkedin} alt="linkedin" width={25} height={25} />
              </a>
            </div>
            <div>
              <a href="/">
                <Image src={mail} alt="mail" width={25} height={25} />
              </a>
              <a href="/">
                <Image src={twitter} alt="twitter" width={25} height={25} />
              </a>
            </div>
          </div>
        </AnimateSection>
        <AnimateSection
          initial={{ opacity: 0, y: 20 }}
          whileInView={{ opacity: 1, y: 0 }}
          transition={{ duration: 0.9, delay: 0.6 }}
          viewport={{ amount: 0.3 }}
        >
          <div>
            <ul>
              <li>
                <Link href={'/'}>HOME</Link>
              </li>
              <li>
                <Link href={'/digitaltwin'}>DIGITAL TWIN</Link>
              </li>
              <li>
                <Link href={'/industria'}>INDUSTRY 5.0</Link>
              </li>
              <li>
                <Link href={'/servizi'}>SERVIZI</Link>
              </li>
              <li>
                <Link href={'/certificazioni'}>CERTIFICAZIONI</Link>
              </li>
              <li>
                <Link href={'/lavora-con-noi'}>CANDIDATI</Link>
              </li>
              <li>
                <Link href={'/contatti'}>CONTATTI</Link>
              </li>
            </ul>
          </div>
        </AnimateSection>
        <AnimateSection
          initial={{ opacity: 0, y: 20 }}
          whileInView={{ opacity: 1, y: 0 }}
          transition={{ duration: 0.9, delay: 0.8 }}
          viewport={{ amount: 0.3 }}
        >
          <div>
            <p>
              © Codematica srl<br></br> P.IVA 02144140460<br></br>
              <Link href="https://backend.codematica.it/wp-content/uploads/2024/05/Bilancio-Sociale-2023.pdf">
                Bilancio Sociale
              </Link>{' '}
              '<Link href="https://xdesigners.it">Credits: xdesigners.it</Link>
            </p>
          </div>
        </AnimateSection>
      </div>
      <div className={`${!absolute && '!h-auto py-10'} footer__white`}>
        <AnimateSection
          initial={{ opacity: 0, y: 20 }}
          whileInView={{ opacity: 1, y: 0 }}
          transition={{ duration: 0.9, delay: 0 }}
          viewport={{ amount: 0.3 }}
        >
          <div>
            <Image src={bandobanner} alt="" />
          </div>
        </AnimateSection>
        <AnimateSection
          initial={{ opacity: 0, y: 20 }}
          whileInView={{ opacity: 1, y: 0 }}
          transition={{ duration: 0.9, delay: 0.2 }}
          viewport={{ amount: 0.3 }}
        >
          <div>
            <Link
              href={
                'https://backend.codematica.it/wp-content/uploads/2024/05/LeatherUp.pdf'
              }
            >
              <Image src={leatherUp} alt="" />
            </Link>
          </div>
        </AnimateSection>
        <AnimateSection
          initial={{ opacity: 0, y: 20 }}
          whileInView={{ opacity: 1, y: 0 }}
          transition={{ duration: 0.9, delay: 0.4 }}
          viewport={{ amount: 0.3 }}
        >
          <div>
            <Link
              href={
                'https://backend.codematica.it/wp-content/uploads/2024/05/Politica_ISO9001.pdf'
              }
            >
              <Image
                src={
                  'https://backend.codematica.it/wp-content/uploads/2024/05/CQY_14.15_45.18_RGB.png'
                }
                alt=""
                width={125}
                height={125}
              />
            </Link>
          </div>
        </AnimateSection>
      </div>
    </footer>
  )
}

export default Footer
