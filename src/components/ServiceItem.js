'use client'

import { Suspense } from 'react'

import { GET_SINGLE_SERVIZIO } from '@/graphql/query'
import { useSuspenseQuery } from '@apollo/client'
import Header from './Header'
import Footer from './Footer'

const ServiceItem = params => {
  const { data, error } = useSuspenseQuery(GET_SINGLE_SERVIZIO, {
    variables: { id: params?.slug, language: params?.lang.toUpperCase() },
  })

  if (error) return <div>Errore</div>
  return (
    <Suspense fallback={'loading'}>
      <main className={` flex justify-center `}>
        <Header lang={params.lang} />
        <div className="container min-h-screen flex flex-col justify-start mt-32 px-10">
          <img
            src={data?.servizio?.featuredImage?.node?.sourceUrl}
            alt={data?.servizio?.translation?.title || data?.servizio?.title}
          />
          <h1
            className="text-4xl font-bold mt-5"
            dangerouslySetInnerHTML={{
              __html:
                data?.servizio?.translation?.title || data?.servizio?.title,
            }}
          ></h1>
          <p
            className="mt-4"
            dangerouslySetInnerHTML={{
              __html:
                data?.servizio?.translation?.content || data?.servizio?.content,
            }}
          ></p>
        </div>
      </main>
      <Footer absolute={false} />
    </Suspense>
  )
}

export default ServiceItem
