import { useEffect, useRef } from 'react'
import * as THREE from 'three'
import { SVGLoader } from 'three/addons/loaders/SVGLoader.js'

const SvgShape = ({ svg, ...props }) => {
  const loader = new SVGLoader()
  const mesh = useRef()

  useEffect(() => {
    loader.load('../../public/images/test.svg', function (data) {
      const paths = data.paths
      console.log('SHAPE', paths)
      for (let j = 0; j < shape.length; j++) {
        const shape = shape[j]
        const geometry = new THREE.ShapeGeometry(shape)
        ref.mesh.ShapeGeometry(shape)
        const mesh = new THREE.Mesh(geometry, material)
        group.add(mesh)
      }
    })
  }, [])
  return (
    <mesh position={props.position} ref={mesh}>
      <meshBasicMaterial
        attach="material"
        color={new THREE.Color('skyblue')}
        opacity={1}
        side={THREE.DoubleSide}
        polygonOffset
        depthWrite={false}
      />
      <shapeBufferGeometry attach="geometry" />
    </mesh>
  )
}

export default SvgShape
