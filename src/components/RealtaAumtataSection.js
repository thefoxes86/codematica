import { cerificazioni, realta } from '@/utils/mock'
import demoImage from '../../public/images/demo.jpeg'
import Image from 'next/image'
import { useFrame } from '@react-three/fiber'
import { useScroll } from '@react-three/drei'
import { useState } from 'react'
import { useMediaQuery } from '../hooks/useMediaQuery'
import AnimateSection from './AnimateSection'

const RealtaAumtataSection = ({
  topMd,
  topLg,
  topXl,
  position,
  pages,
  svg,
}) => {
  const isMd = useMediaQuery('(max-width: 768px)')
  const isLg = useMediaQuery('(max-width: 1024px)')
  const data = useScroll()
  const [topAbs, setTopAbs] = useState(0)

  useFrame(() => {
    setTopAbs(data.range(position / pages, position / pages))
  })
  return (
    <div
      className="internal_realta__section"
      style={{ top: isMd ? topMd : isLg ? topLg : topXl }}
    >
      {realta.map(item => (
        <AnimateSection
          initial={{ opacity: 0, y: 20 }}
          whileInView={{ opacity: 1, y: 0 }}
          transition={{ duration: 0.9, delay: 0 }}
          viewport={{ amount: 0.3 }}
          className="internal_realta__item"
        >
          <span className="number">{item.number}</span>
          <div className="content">
            <h3
              className="title"
              dangerouslySetInnerHTML={{ __html: item.title }}
            ></h3>
            <Image src={item.img} width={1000} height={300} alt="" />

            <div
              className="description"
              dangerouslySetInnerHTML={{ __html: item.text }}
            ></div>
          </div>
        </AnimateSection>
      ))}
      {svg && (
        <Image
          src={svg}
          alt=""
          style={{ transform: `translateY(-${topAbs * 450}px)` }}
        />
      )}
    </div>
  )
}

export default RealtaAumtataSection
