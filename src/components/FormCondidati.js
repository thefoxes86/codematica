import { useForm } from 'react-hook-form'

import Image from 'next/image'
import { useFrame } from '@react-three/fiber'
import { useScroll } from '@react-three/drei'
import { useState } from 'react'
import Button from './Button'
import { useMediaQuery } from '../hooks/useMediaQuery'
import AnimateSection from './AnimateSection'

const FormCondidati = ({ topMd, topLg, topXl, position, pages, svg }) => {
  const isMd = useMediaQuery('(max-width: 768px)')
  const isLg = useMediaQuery('(max-width: 1024px)')
  const data = useScroll()
  const [topAbs, setTopAbs] = useState(0)
  const {
    register,
    handleSubmit,
    watch,
    formState: { errors },
  } = useForm()
  const onSubmit = data => console.log(data)

  useFrame(() => {
    setTopAbs(data.range(position / pages, position / pages))
  })
  return (
    <div
      className="form_contact__section"
      style={{ top: isMd ? topMd : isLg ? topLg : topXl }}
    >
      <form onSubmit={handleSubmit(onSubmit)}>
        <div className="form__row">
          <AnimateSection
            initial={{ opacity: 0, y: 20 }}
            whileInView={{ opacity: 1, y: 0 }}
            transition={{ duration: 0.9, delay: 0 }}
            viewport={{ amount: 0.3 }}
            className="form__row__item"
          >
            <label>Nome</label>
            <input type="text" {...register('nome', { required: true })} />
            {errors.exampleRequired && <span>This field is required</span>}
          </AnimateSection>
          <AnimateSection
            initial={{ opacity: 0, y: 20 }}
            whileInView={{ opacity: 1, y: 0 }}
            transition={{ duration: 0.9, delay: 0 }}
            viewport={{ amount: 0.3 }}
            className="form__row__item"
          >
            <label>eMail</label>
            <input type="email" {...register('email', { required: true })} />
            {errors.exampleRequired && <span>This field is required</span>}
          </AnimateSection>
          <AnimateSection
            initial={{ opacity: 0, y: 20 }}
            whileInView={{ opacity: 1, y: 0 }}
            transition={{ duration: 0.9, delay: 0 }}
            viewport={{ amount: 0.3 }}
            className="form__row__item"
          >
            <label>Curriculum</label>
            <input type="file" {...register('file')} />
          </AnimateSection>
        </div>

        <div className="form__row">
          <AnimateSection
            initial={{ opacity: 0, y: 20 }}
            whileInView={{ opacity: 1, y: 0 }}
            transition={{ duration: 0.9, delay: 0 }}
            viewport={{ amount: 0.3 }}
            className="form__row__item"
          >
            <label>Messaggio</label>
            <textarea {...register('messaggio', { required: true })} />
            {errors.exampleRequired && <span>Questo campo è obbligatorio</span>}
          </AnimateSection>
        </div>
        <AnimateSection
          initial={{ opacity: 0, y: 20 }}
          whileInView={{ opacity: 1, y: 0 }}
          transition={{ duration: 0.9, delay: 0 }}
          viewport={{ amount: 0.3 }}
          className="form__row"
        >
          <div className="form__row__item  text-right">
            <Button type="submit">Invia</Button>
          </div>
        </AnimateSection>
      </form>
      {svg && (
        <Image
          src={svg}
          alt=""
          style={{ transform: `translateY(-${topAbs * 450}px)` }}
        />
      )}
    </div>
  )
}

export default FormCondidati
