'use client'

import Link from 'next/link'
import { usePathname } from 'next/navigation'
import { i18n } from 'i18n.config'
import '/node_modules/flag-icons/css/flag-icons.min.css'

export default function LocalSwitcher() {
  const pathName = usePathname()

  const redirectedPathName = locale => {
    if (!pathName) return '/'
    const segments = pathName.split('/')
    segments[1] = locale
    return segments.join('/')
  }

  return (
    <ul className="flex gap-x-3">
      {i18n.locales.map(locale => {
        return (
          <li key={locale}>
            <Link
              href={redirectedPathName(locale)}
              className="flex  bg-black px-1 text-white text-xs w-10"
            >
              <span
                className={`fi fi-${
                  locale === 'en' ? 'gb' : locale === 'ar' ? 'ae' : locale
                }`}
              ></span>
            </Link>
          </li>
        )
      })}
    </ul>
  )
}
