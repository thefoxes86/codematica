import Link from 'next/link'
import Image from 'next/image'
import logo from '../../public/images/Codematica2020.svg'
import logoBlack from '../../public/images/logonero.svg'
import useBearStore from '@/context/zustand'

const LogoHeader = () => {
  const logoLight = useBearStore(state => state.logoLight)

  return (
    <Link href={'/'}>
      <Image
        src={logoLight ? logo : logoBlack}
        id="logo"
        width={'auto'}
        height={70}
        priority
        alt="Logo Codematica"
      />
    </Link>
  )
}

export default LogoHeader
