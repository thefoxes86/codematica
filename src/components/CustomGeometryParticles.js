import { useFrame } from '@react-three/fiber'
import { useMemo, useRef } from 'react'
import * as THREE from 'three'
import vertexShader from '@/shaders/vertexShader'
import fragmentShader from '@/shaders/fragmentShader'

const CustomGeometryParticles = props => {
  const { count } = props
  const radius = props.radius || 3

  // This reference gives us direct access to our points
  const points = useRef()

  // Generate our positions attributes array
  const particlesPosition = useMemo(() => {
    const positions = new Float32Array(count * 3)

    for (let i = 0; i < count; i++) {
      const distance = Math.sqrt(Math.random()) * radius
      const theta = THREE.MathUtils.randFloatSpread(360)
      const phi = THREE.MathUtils.randFloatSpread(360)

      let x = distance * Math.sin(theta) * Math.cos(phi)
      let y = distance * Math.sin(theta) * Math.sin(phi)
      let z = distance * Math.cos(theta)

      positions.set([x, y, z], i * 3)
    }

    return positions
  }, [count])

  const uniforms = useMemo(
    () => ({
      uTime: {
        value: 0.0,
      },
      uRadius: {
        value: radius,
      },
      uColorPrimary: {
        type: 'c',
        value: new THREE.Color(props.colorPrimary || '#FB1A0C'),
      },
      uColorSecondary: {
        tyape: 'c',
        value: new THREE.Color(props.colorSecondary || '#FB1A0C'),
      },
      uDesnsity: {
        value: props.density || 2.5,
      },
    }),
    []
  )

  useFrame(state => {
    const { clock } = state

    points.current.material.uniforms.uTime.value = clock.elapsedTime
  })

  return (
    <points ref={points} position={props.position}>
      <bufferGeometry>
        <bufferAttribute
          attach="attributes-position"
          count={particlesPosition.length / 3}
          array={particlesPosition}
          itemSize={3}
        />
      </bufferGeometry>
      <shaderMaterial
        blending={THREE.AdditiveBlending}
        depthWrite={false}
        fragmentShader={fragmentShader}
        vertexShader={vertexShader}
        uniforms={uniforms}
      />
    </points>
  )
}

export default CustomGeometryParticles
