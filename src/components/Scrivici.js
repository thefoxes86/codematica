import Link from 'next/link'
import { useMediaQuery } from '../hooks/useMediaQuery'
import AnimateSection from './AnimateSection'

const Scrivici = ({ topMd, topLg, topXl }) => {
  const isMd = useMediaQuery('(max-width: 768px)')
  const isLg = useMediaQuery('(max-width: 1024px)')
  return (
    <div
      className="green_section"
      style={{ top: isMd ? topMd : isLg ? topLg : topXl }}
    >
      <AnimateSection
        initial={{ opacity: 0, y: 20 }}
        whileInView={{ opacity: 1, y: 0 }}
        transition={{ duration: 0.9, delay: 0 }}
        viewport={{ amount: 0.3 }}
      >
        <h2>SCRIVICI</h2>
      </AnimateSection>
      <AnimateSection
        initial={{ opacity: 0, y: 20 }}
        whileInView={{ opacity: 1, y: 0 }}
        transition={{ duration: 0.9, delay: 0 }}
        viewport={{ amount: 0.3 }}
      >
        <p className="md:ml-20 ml-2 my-10">
          Se vuoi sviluppare un progetto personalizzato contattaci
        </p>
      </AnimateSection>
      <AnimateSection
        initial={{ opacity: 0, y: 20 }}
        whileInView={{ opacity: 1, y: 0 }}
        transition={{ duration: 0.9, delay: 0 }}
        viewport={{ amount: 0.3 }}
      >
        <Link href={'/contatti'} className="btn md:ml-20 ml-2">
          Leggi di più
        </Link>
      </AnimateSection>
    </div>
  )
}

export default Scrivici
