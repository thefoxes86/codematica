import Link from 'next/link'
import Image from 'next/image'
import { useFrame } from '@react-three/fiber'
import { useScroll } from '@react-three/drei'
import { useState } from 'react'
import { useMediaQuery } from '../hooks/useMediaQuery'
import AnimateSection from './AnimateSection'

const IndustrySection = ({ topMd, topLg, topXl, position, pages, svg }) => {
  const isMd = useMediaQuery('(max-width: 768px)')
  const isLg = useMediaQuery('(max-width: 1024px)')
  const data = useScroll()
  const [topAbs, setTopAbs] = useState(0)

  useFrame(() => {
    setTopAbs(data.range(position / pages, position / pages))
  })
  return (
    <div
      className="industry_section"
      style={{ top: isMd ? topMd : isLg ? topLg : topXl }}
    >
      {/* <Image src={svgWorld} alt="Logo" className="svgPath" /> */}
      <AnimateSection
        initial={{ opacity: 0, y: 20 }}
        whileInView={{ opacity: 1, y: 0 }}
        transition={{ duration: 0.9, delay: 0 }}
        viewport={{ amount: 0.3 }}
      >
        <h2>INDUSTRY 5.0</h2>
      </AnimateSection>
      <AnimateSection
        initial={{ opacity: 0, y: 20 }}
        whileInView={{ opacity: 1, y: 0 }}
        transition={{ duration: 0.9, delay: 0 }}
        viewport={{ amount: 0.3 }}
      >
        <p className="md:ml-20 ml-2 my-10">
          Permette l’unificazione, il controllo e così l’ottimizzazione dei vari
          macchinari automatizzati. <br></br>
          <span className="w-100 d-block font-bold">
            Codematica crede e investe nell’Industry 5.0.
          </span>
        </p>
      </AnimateSection>
      <AnimateSection
        initial={{ opacity: 0, y: 20 }}
        whileInView={{ opacity: 1, y: 0 }}
        transition={{ duration: 0.9, delay: 0 }}
        viewport={{ amount: 0.3 }}
      >
        <Link href={'/industry'} className="btn md:ml-20 ml-2">
          Leggi di più
        </Link>
      </AnimateSection>
      {svg && (
        <Image
          src={svg}
          alt=""
          style={{
            transform: `translateY(-${topAbs * 450}px) translateX(-200px)`,
          }}
        />
      )}
    </div>
  )
}

export default IndustrySection
