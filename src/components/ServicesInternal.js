import { services } from '@/utils/mock'
import demoImage from '../../public/images/demo.jpeg'
import Image from 'next/image'
import { useFrame } from '@react-three/fiber'
import { useScroll } from '@react-three/drei'
import { useEffect, useState } from 'react'
import { useMediaQuery } from '../hooks/useMediaQuery'
import AnimateSection from './AnimateSection'
import Link from 'next/link'

const ServicesInternal = ({
  topMd,
  topLg,
  topXl,
  position,
  pages,
  svg,
  servizi,
  lang,
}) => {
  const isMd = useMediaQuery('(max-width: 768px)')
  const isLg = useMediaQuery('(max-width: 1024px)')
  const data = useScroll()
  const [topAbs, setTopAbs] = useState(0)
  const [serviceListProps, setServiceListProps] = useState([])

  const toggleDescriptionModal = index => {
    let newServiceListProps = [...serviceListProps]
    newServiceListProps[index].isOpen = !newServiceListProps[index].isOpen
    setServiceListProps(newServiceListProps)
  }
  useEffect(() => {
    let newServicesArray = []
    servizi?.edges.map(index => {
      newServicesArray.push({ isOpen: false })
    })
    setServiceListProps(newServicesArray)
  }, [servizi])

  useFrame(() => {
    setTopAbs(data.range(position / pages, position / pages))
  })
  return (
    <>
      {serviceListProps.length > 0 ? (
        <div
          className="internal_services__section"
          style={{ top: isMd ? topMd : isLg ? topLg : topXl }}
        >
          {servizi?.edges.map(({ node }, index) => (
            <div className="internal_service__item">
              <AnimateSection
                initial={{ opacity: 0, y: 20 }}
                whileInView={{ opacity: 1, y: 0 }}
                transition={{ duration: 0.9, delay: 0 }}
                viewport={{ amount: 0.3 }}
              >
                <span className="number">
                  {index + 1 < 10 ? `0${index + 1}` : index + 1}
                </span>
              </AnimateSection>

              <AnimateSection
                initial={{ opacity: 0, y: 20 }}
                whileInView={{ opacity: 1, y: 0 }}
                transition={{ duration: 0.9, delay: 0 }}
                viewport={{ amount: 0.3 }}
                className="content"
              >
                <h3
                  className="title"
                  dangerouslySetInnerHTML={{ __html: node?.title }}
                ></h3>
                <Image
                  src={node?.featuredImage?.node?.sourceUrl || ''}
                  width={500}
                  height={500}
                  alt=""
                />
                <span className="more_detail">
                  <Link href={`/${lang}/servizi/${node?.slug}`}>+</Link>
                </span>
              </AnimateSection>
            </div>
          ))}
          {svg && (
            <Image
              src={svg}
              alt=""
              style={{ transform: `translateY(-${topAbs * 450}px)` }}
            />
          )}
        </div>
      ) : null}
    </>
  )
}

export default ServicesInternal
