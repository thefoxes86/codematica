import { AnimatePresence, motion, useCycle } from 'framer-motion'

import Link from 'next/link'
import LogoHeader from './LogoHeader'
import LocalSwitcher from './LocalSwitcher'
import CookieConsentBanner from './CookieConsentBanner'

const itemVariants = {
  closed: {
    opacity: 0,
    y: 10,
  },
  open: { opacity: 1, y: 0 },
}

const sideVariants = {
  closed: {
    transition: {
      staggerChildren: 0.1,
      staggerDirection: -1,
    },
  },
  open: {
    transition: {
      staggerChildren: 0.2,
      staggerDirection: 1,
    },
  },
}

const Header = ({ lang }) => {
  const [open, cycleOpen] = useCycle(false, true)

  const links = [
    { id: 0, name: 'Home', to: `/${lang}` },
    { id: 1, name: 'Digital Twin', to: `/${lang}/digitaltwin` },
    { id: 2, name: 'Industria 5.0', to: `/${lang}/industria` },
    { id: 3, name: 'Servizi', to: `/${lang}/servizi` },
    { id: 4, name: 'Certificazioni', to: `/${lang}/certificazioni` },
    { id: 5, name: 'News', to: `/${lang}/news` },
    { id: 6, name: 'Candidati', to: `/${lang}/lavora-con-noi` },
    { id: 7, name: 'Contatti', to: `/${lang}/contatti` },
  ]

  return (
    <header>
      <CookieConsentBanner />
      <div className="header" id="header">
        <div className="header__content">
          <LogoHeader />
          <div
            className={`menu__toggle ${open ? 'opened' : ''}`}
            onClick={cycleOpen}
          >
            <span className="menu__toggle__item"></span>
            <span className="menu__toggle__item"></span>
            <span className="menu__toggle__item"></span>
          </div>
        </div>
      </div>
      <AnimatePresence>
        {open && (
          <motion.aside
            className="menu__container"
            initial={{ right: -350 }}
            animate={{
              right: 0,
            }}
            exit={{
              right: -350,
              transition: { delay: 0.7, duration: 0.3 },
            }}
          >
            <motion.div
              className="menu__items"
              initial="closed"
              animate="open"
              exit="closed"
              variants={sideVariants}
            >
              {links.map(({ name, to, id }) => (
                <motion.div
                  key={id}
                  whileHover={{ scale: 1.1 }}
                  variants={itemVariants}
                  onClick={cycleOpen}
                >
                  <Link href={to}>{name}</Link>
                </motion.div>
              ))}
              <LocalSwitcher />
            </motion.div>
          </motion.aside>
        )}
      </AnimatePresence>
    </header>
  )
}

export default Header
