import Link from 'next/link'
import Image from 'next/image'
import { useFrame } from '@react-three/fiber'
import { useScroll } from '@react-three/drei'
import { useState } from 'react'
import { useMediaQuery } from '../hooks/useMediaQuery'
import AnimateSection from './AnimateSection'

const WorldSection = ({ topMd, topLg, topXl, position, pages, svg }) => {
  const isMd = useMediaQuery('(max-width: 768px)')
  const isLg = useMediaQuery('(max-width: 1024px)')
  const data = useScroll()
  const [topAbs, setTopAbs] = useState(0)

  useFrame(() => {
    setTopAbs(data.range(position / pages, position / pages))
  })
  return (
    <div
      className="world_section"
      style={{ top: isMd ? topMd : isLg ? topLg : topXl }}
    >
      <AnimateSection
        initial={{ opacity: 0, y: 20 }}
        whileInView={{ opacity: 1, y: 0 }}
        transition={{ duration: 0.9, delay: 0 }}
        viewport={{ amount: 0.3 }}
      >
        <h2>AROUND THE WORLD</h2>
      </AnimateSection>
      <AnimateSection
        initial={{ opacity: 0, y: 20 }}
        whileInView={{ opacity: 1, y: 0 }}
        transition={{ duration: 0.9, delay: 0 }}
        viewport={{ amount: 0.3 }}
      >
        <p className="md:ml-20 ml-2 my-10">
          <span className="font-bold">Codematica</span>
          vanta una grossa esperienza internazionale
        </p>
      </AnimateSection>
      <AnimateSection
        initial={{ opacity: 0, y: 20 }}
        whileInView={{ opacity: 1, y: 0 }}
        transition={{ duration: 0.9, delay: 0 }}
        viewport={{ amount: 0.3 }}
      >
        <Link href={'/contatti'} className="btn md:ml-20 ml-2 z-50">
          Unisciti a noi
        </Link>
      </AnimateSection>
      {svg && (
        <Image
          src={svg}
          alt=""
          style={{ transform: `translateY(-${topAbs * 450}px)` }}
        />
      )}
    </div>
  )
}

export default WorldSection
