import Image from 'next/image'
import { useFrame } from '@react-three/fiber'
import { useState } from 'react'
import { useScroll } from '@react-three/drei'
import { useMediaQuery } from '../hooks/useMediaQuery'
import AnimateSection from './AnimateSection'

const IconSection = ({ topMd, topLg, topXl, position, pages, svg }) => {
  const isMd = useMediaQuery('(max-width: 768px)')
  const isLg = useMediaQuery('(max-width: 1024px)')
  const data = useScroll()
  const [topAbs, setTopAbs] = useState(0)

  useFrame(() => {
    setTopAbs(data.range(position / pages, position / pages))
  })
  return (
    <>
      <AnimateSection
        initial={{ opacity: 0, y: 20 }}
        whileInView={{ opacity: 1, y: 0 }}
        transition={{ duration: 0.9, delay: 0 }}
        viewport={{ amount: 0.3 }}
        className="icon_section"
        style={{ top: isMd ? topMd : isLg ? topLg : topXl }}
      >
        {svg && (
          <Image
            src={svg}
            alt=""
            style={{ transform: `translateY(-${topAbs * 450}px)` }}
          />
        )}
      </AnimateSection>
    </>
  )
}

export default IconSection
