import Link from 'next/link'
import { useFrame } from '@react-three/fiber'
import { useState } from 'react'
import { useScroll } from '@react-three/drei'
import Image from 'next/image'

const TitleSection = ({ top, title, svg, position, pages }) => {
  const data = useScroll()
  const [topAbs, setTopAbs] = useState(0)

  useFrame(() => {
    setTopAbs(data.range(0, (position / pages) * 2))
  })

  return (
    <div className="title_section" style={{ top: top }}>
      {/* <Image src={svgWorld} alt="Logo" className="svgPath" /> */}
      <h2>{title}</h2>
      {svg && (
        <Image
          src={svg}
          alt=""
          style={{
            width: '100vw',
            position: 'absolute',
            transform: `translateY(-${topAbs * 320}px)`,
            top: '0',
            left: '0',
          }}
        />
      )}
    </div>
  )
}

export default TitleSection
