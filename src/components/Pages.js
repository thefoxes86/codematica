import { useThree, useFrame } from '@react-three/fiber'
import Page from './Page'
import Cloud from './Cloud'

import { Fragment, useEffect, useRef } from 'react'
import { useScroll } from '@react-three/drei'
import ScrollDown from './ScrollDown'
import useBearStore from '@/context/zustand'
import CustomGeometryParticles from './CustomGeometryParticles'
import { useMediaQuery } from '@/hooks/useMediaQuery'

const Pages = ({ type }) => {
  const { height, width } = useThree(state => state.viewport)
  const camera = useThree(state => state.camera)
  const [logoLight, cycleLogoLight, serviceListProps] = useBearStore(state => [
    state.logoLight,
    state.cycleLogoLight,
    state.serviceListProps,
  ])
  const isXs = useMediaQuery('(max-width: 768px)')

  const data = useScroll()
  const header = document.getElementById('header')

  const menu__toggle__item = [
    ...document.getElementsByClassName('menu__toggle__item'),
  ]

  useEffect(() => {
    console.log('top', serviceListProps.top)
  }, [serviceListProps.top])

  useFrame(() => {
    const a = data.range(0, 1 / 8)
    let repeatAction = true

    if (a >= 1 && header?.classList.contains('scrolled') === false) {
      header?.classList.add('scrolled')
      logoLight && cycleLogoLight()
      menu__toggle__item?.map(item => {
        return item?.classList.add('scrolled')
      })
    }
    if (a < 1) {
      header.classList.remove('scrolled')
      !logoLight && cycleLogoLight()
      menu__toggle__item?.map(item => {
        return item?.classList.remove('scrolled')
      })
    }
  })

  return (
    <Fragment>
      {type?.map(({ section, position }, index) => (
        <Fragment key={index}>
          {/* WORD CLOUD */}
          {section === 'cloud' && (
            <Page position={[0, height * position, 0]} key={section}>
              {/* <fog attach="fog" args={['#202025', 0, 80]} /> */}
              <Cloud count={8} radius={20} />
              {/* <ScrollDown /> */}
            </Page>
          )}

          {/* SERVICES */}
          {section === 'services' || section === 'services_internal' ? (
            <Page position={[0, height * -position, 0]}>
              <group>
                <CustomGeometryParticles
                  position={[isXs ? 2 : 4, -1, 3]}
                  colorPrimary={'#C62A20'}
                  colorSecondary={'#C01D00'}
                  density={5}
                  count={4000}
                />

                <CustomGeometryParticles
                  position={[isXs ? 1.5 : -4.5, -4, 2]}
                  colorPrimary={'#003776'}
                  colorSecondary={'#0076FF'}
                  density={5}
                  count={4000}
                />
              </group>
            </Page>
          ) : null}
          {/* SERVICES SWIPER LIST*/}
          {section === 'services_list' && (
            <Page position={[0, height * -position, 0]}>
              <group>
                <CustomGeometryParticles
                  position={[isXs ? -1 : -3, 0, 1]}
                  colorPrimary={'#C62A20'}
                  colorSecondary={'#C01D00'}
                  density={3}
                  count={1000}
                />

                <CustomGeometryParticles
                  position={[isXs ? 0.5 : 2.5, 2, 3]}
                  colorPrimary={'#003776'}
                  colorSecondary={'#0076FF'}
                  density={1}
                  count={2000}
                />
              </group>
            </Page>
          )}

          {/* ICON SECTION*/}
          {section === 'icon' && (
            <Page position={[0, height * -position, 0]}>
              <group>
                <CustomGeometryParticles
                  position={[isXs ? -1.5 : -4, 1, 1]}
                  colorPrimary={'#760068'}
                  colorSecondary={'#cc0289'}
                  density={4}
                  count={8000}
                />
              </group>
            </Page>
          )}
          {/* INDUSTRY SECTION*/}
          {section === 'industry' && (
            <Page position={[0, height * -position, 0]}>
              <group>
                <CustomGeometryParticles
                  position={[isXs ? 0.5 : -1, 1, 2]}
                  colorPrimary={'#003776'}
                  colorSecondary={'#0076FF'}
                  density={3}
                  count={3000}
                />
              </group>
            </Page>
          )}
          {/* WORLD SECTION*/}
          {section === 'world' && (
            <Page position={[0, height * -position, 0]}>
              <group>
                <CustomGeometryParticles
                  position={[isXs ? 1 : 4.5, -1, 2]}
                  colorPrimary={'#C62A20'}
                  colorSecondary={'#C01D00'}
                  density={3}
                  count={4000}
                />
              </group>
            </Page>
          )}
          {/* GREEN SECTION*/}
          {section === 'green' && (
            <Page position={[0, height * -position, 0]}>
              <group>
                <CustomGeometryParticles
                  position={[isXs ? -1 : -4, -4, 1]}
                  colorPrimary={'#2fe028'}
                  colorSecondary={'#06c200'}
                  density={4}
                  count={6000}
                />
              </group>
            </Page>
          )}
          {/* SCRIVICI SECTION*/}
          {section === 'scrivici' && (
            <Page position={[0, height * -position, 0]}>
              <group>
                <CustomGeometryParticles
                  position={[isXs ? 1 : 4, 2, 1]}
                  colorPrimary={'#2fe028'}
                  colorSecondary={'#06c200'}
                  density={1.2}
                  count={1000}
                />
              </group>
            </Page>
          )}
          {/* CERTIFICAZIONI SECTION*/}
          {section === 'certificazioni' && (
            <Page position={[0, height * -position, 0]}>
              <group>
                <CustomGeometryParticles
                  position={[isXs ? 1.5 : 4, 4, 2]}
                  colorPrimary={'#003776'}
                  colorSecondary={'#0076FF'}
                  density={3.2}
                  count={2000}
                />
              </group>
            </Page>
          )}
        </Fragment>
      ))}
    </Fragment>
  )
}

export default Pages
