// Import Swiper React components
import { Swiper, SwiperSlide, useSwiper } from 'swiper/react'
// Import Swiper styles
import 'swiper/css'
import 'swiper/css/pagination'
import { EffectFade, Mousewheel, Pagination } from 'swiper/modules'
import { useRef } from 'react'
import demoImage from '../../public/images/demo.jpeg'
import { useFrame } from '@react-three/fiber'
import { useScroll } from '@react-three/drei'
import { useState } from 'react'
// Import Swiper styles
import 'swiper/css'
import 'swiper/css/effect-fade'
import Button from './Button'
import Image from 'next/image'
import { services } from '@/utils/mock'
import useBearStore from '@/context/zustand'
import { gsap } from 'gsap'
import TitleSection from './TitleSection'
import { useMediaQuery } from '../hooks/useMediaQuery'
import Link from 'next/link'

const SingleService = ({
  topMd,
  topLg,
  topXl,
  position,
  pages,
  svg,
  servizi,
  lang,
}) => {
  const isMd = useMediaQuery('(max-width: 768px)')
  const isLg = useMediaQuery('(max-width: 1024px)')
  const isXl = useMediaQuery('(min-width: 1025px)')
  const ref = useRef()
  const swiper = useSwiper()

  const data = useScroll()
  const [topAbs, setTopAbs] = useState(0)
  const [setServiceListProps] = useBearStore(state => [
    state.setServiceListProps,
  ])

  useFrame(() => {
    setTopAbs(data.range(position / pages, position / pages))
  })
  const lastSlideCallback = swiper => {
    if (swiper.isEnd && isXl) {
      //   ref.current.style.pointerEvents = 'none'
      swiper.mousewheel.disable()
      setTimeout(() => {
        swiper.slideTo(0)
        swiper.mousewheel.enable()
      }, 8000)
    }
  }
  const scrollControlls = document.getElementById('scroll-html')

  const centerScreen = e => {
    gsap.to(scrollControlls, {
      transform: `translate3d(0px, -${ref.current.offsetTop - 100}px, 0px)`,
      duration: 1,
      ease: 'power4.out',
    })
  }
  return (
    <div
      ref={ref}
      className="swiper-container"
      style={{ top: isMd ? topMd : isLg ? topLg : topXl }}
      onMouseEnter={centerScreen}
    >
      <Swiper
        onSlideChange={lastSlideCallback}
        slidesPerView={1}
        spaceBetween={30}
        mousewheel={isXl ? true : false}
        pagination={{
          clickable: true,
        }}
        modules={[Mousewheel, Pagination]}
        className="mySwiper"
      >
        {servizi?.edges.map(({ node }, index) => (
          <SwiperSlide key={index}>
            <div className="container__item">
              <div className="container__item__header">
                <h1>{index + 1 < 10 ? `0${index + 1}` : index + 1}</h1>
                <h2 dangerouslySetInnerHTML={{ __html: node?.title }}></h2>
              </div>
              <div className="container__item__content">
                <div
                  className="container_text"
                  dangerouslySetInnerHTML={{ __html: node?.content }}
                ></div>

                <Image
                  src={node?.featuredImage?.node?.sourceUrl || ''}
                  width={500}
                  height={500}
                  alt={node?.title}
                />
              </div>
              <Button>
                <Link href={`/${lang}/servizi/${node?.slug}`}>
                  SCOPRI IL SERVIZIO
                </Link>
              </Button>
            </div>
          </SwiperSlide>
        ))}
      </Swiper>
      {/* <Button
          
        style={{
          position: 'absolute',
          right: 10,
          bottom: -60,
          zIndex: 1000,
        }}
      >
        Salta i servizi
      </Button> */}

      {svg && (
        <Image
          src={svg}
          alt=""
          style={{ transform: `translateY(-${topAbs * 450}px)` }}
        />
      )}
    </div>
  )
}

export default SingleService
