import { useFrame } from '@react-three/fiber'
import { useScroll } from '@react-three/drei'
import { useState } from 'react'
import Link from 'next/link'
import { useMediaQuery } from '../hooks/useMediaQuery'
import AnimateSection from './AnimateSection'

const CertificazioniSection = ({ topMd, topLg, topXl }) => {
  const isMd = useMediaQuery('(max-width: 768px)')
  const isLg = useMediaQuery('(max-width: 1024px)')
  return (
    <div
      className="green_section"
      style={{ top: isMd ? topMd : isLg ? topLg : topXl }}
    >
      <AnimateSection
        initial={{ opacity: 0, y: 20 }}
        whileInView={{ opacity: 1, y: 0 }}
        transition={{ duration: 0.9, delay: 0 }}
        viewport={{ amount: 0.3 }}
      >
        <h2>CERTIFICAZIONI</h2>
      </AnimateSection>
      <AnimateSection
        initial={{ opacity: 0, y: 20 }}
        whileInView={{ opacity: 1, y: 0 }}
        transition={{ duration: 0.9, delay: 0 }}
        viewport={{ amount: 0.3 }}
      >
        <p className="md:ml-20 ml-2 my-10">
          Siamo un’azienda che rispetta gli standard di sicurezza e<br></br>{' '}
          qualità e offre servizi di formazione per ottenere le stesse<br></br>{' '}
          certificazioni.
        </p>
      </AnimateSection>
      <AnimateSection
        initial={{ opacity: 0, y: 20 }}
        whileInView={{ opacity: 1, y: 0 }}
        transition={{ duration: 0.9, delay: 0 }}
        viewport={{ amount: 0.3 }}
      >
        <Link href={'/certificazioni'} className="btn md:ml-20 ml-2">
          Scopri Quali
        </Link>
      </AnimateSection>
    </div>
  )
}

export default CertificazioniSection
