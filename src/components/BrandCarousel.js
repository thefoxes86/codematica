import { Swiper, SwiperSlide, useSwiper } from 'swiper/react'
import 'swiper/css'
import 'swiper/css/pagination'
import { useRef } from 'react'
import Image from 'next/image'
import { useFrame } from '@react-three/fiber'
import { useScroll } from '@react-three/drei'
import { useState } from 'react'
import { useMediaQuery } from '../hooks/useMediaQuery'

const BrandCarousel = ({
  topMd,
  topLg,
  topXl,
  position,
  pages,
  svg,
  brands,
}) => {
  const ref = useRef()
  const data = useScroll()
  const [topAbs, setTopAbs] = useState(0)
  const isMd = useMediaQuery('(max-width: 768px)')
  const isLg = useMediaQuery('(max-width: 1024px)')

  useFrame(() => {
    setTopAbs(data.range(position / pages, position / pages))
  })
  return (
    <div
      ref={ref}
      className="brand_carousel_section"
      style={{ top: isMd ? topMd : isLg ? topLg : topXl }}
    >
      <Swiper
        breakpoints={{
          640: {
            slidesPerView: 1,
            spaceBetween: 20,
          },
          768: {
            slidesPerView: 2,
            spaceBetween: 20,
          },
          1024: {
            slidesPerView: 3,
            spaceBetween: 30,
          },
        }}
        spaceBetween={30}
        pagination={{
          clickable: true,
        }}
        className="mySwiperBrand"
      >
        {brands?.edges?.map(({ node }, index) => (
          <SwiperSlide key={index}>
            <Image
              src={node?.featuredImage?.node?.sourceUrl || ''}
              width={100}
              height={100}
              alt={node?.title}
            />
          </SwiperSlide>
        ))}
      </Swiper>
      {svg && (
        <Image
          src={svg}
          alt=""
          style={{ transform: `translateY(-${topAbs * 450}px)` }}
        />
      )}
    </div>
  )
}

export default BrandCarousel
