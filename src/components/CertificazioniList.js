import { cerificazioni, services } from '@/utils/mock'
import demoImage from '../../public/images/demo.jpeg'
import Image from 'next/image'
import { useFrame } from '@react-three/fiber'
import { useScroll } from '@react-three/drei'
import { useState } from 'react'
import { useMediaQuery } from '../hooks/useMediaQuery'
import AnimateSection from './AnimateSection'

const CertificazioniList = ({
  topMd,
  topLg,
  topXl,
  position,
  pages,
  svg,
  contents,
}) => {
  const isMd = useMediaQuery('(max-width: 768px)')
  const isLg = useMediaQuery('(max-width: 1024px)')
  const data = useScroll()
  const [topAbs, setTopAbs] = useState(0)

  useFrame(() => {
    setTopAbs(data.range(position / pages, position / pages))
  })
  return (
    <div
      className="internal_certification__section"
      style={{ top: isMd ? topMd : isLg ? topLg : topXl }}
    >
      {console.log(
        'mostra campo',
        contents.mostraCampo1,
        contents?.immagine1?.node?.sourceUrl
      )}
      {contents.mostraCampo1 ? (
        <AnimateSection
          key={'cert1'}
          initial={{ opacity: 0, y: 20 }}
          whileInView={{ opacity: 1, y: 0 }}
          transition={{ duration: 0.9, delay: 0 }}
          viewport={{ amount: 0.3 }}
          className="internal_certification__item"
        >
          <span className="number">{contents.titolo1}</span>
          <div className="content">
            <h3
              className="title"
              dangerouslySetInnerHTML={{ __html: contents.titolo2 }}
            ></h3>
            <Image
              src={contents?.immagine1?.node?.sourceUrl}
              width={500}
              height={300}
              alt=""
            />

            <div
              className="description"
              dangerouslySetInnerHTML={{ __html: contents.testo1 }}
            ></div>
          </div>
        </AnimateSection>
      ) : null}
      {contents?.mostraCampo2 ? (
        <AnimateSection
          key={'cert2'}
          initial={{ opacity: 0, y: 20 }}
          whileInView={{ opacity: 1, y: 0 }}
          transition={{ duration: 0.9, delay: 0 }}
          viewport={{ amount: 0.3 }}
          className="internal_certification__item"
        >
          <span className="number">{contents.titolo3}</span>
          <div className="content">
            <h3
              className="title"
              dangerouslySetInnerHTML={{ __html: contents.titolo4 }}
            ></h3>
            <Image
              src={contents?.immagine2?.node?.sourceUrl}
              width={500}
              height={300}
              alt=""
            />

            <div
              className="description"
              dangerouslySetInnerHTML={{ __html: contents.testo2 }}
            ></div>
          </div>
        </AnimateSection>
      ) : null}
      {svg && (
        <Image
          src={svg}
          alt=""
          style={{ transform: `translateY(-${topAbs * 450}px)` }}
        />
      )}
    </div>
  )
}

export default CertificazioniList
