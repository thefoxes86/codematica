import Image from 'next/image'
import arrowDown from '../../public/images/iconmonstr-arrow-down-thin.svg'
import { motion } from 'framer-motion'
import { useEffect, useRef, useState } from 'react'
import { Html, useScroll } from '@react-three/drei'
import { useFrame } from '@react-three/fiber'

const ScrollDown = () => {
  const data = useScroll()
  const [visible, setVisible] = useState(1)
  const ref = useRef()

  useFrame(() => {
    const a = data.range(0, 1 / 60)

    setVisible(1 - a)
  })

  return (
    <Html>
      <motion.div
        ref={ref}
        className="scroll-down"
        style={{ opacity: visible }}
      >
        <Image
          src={arrowDown}
          priority
          width={40}
          height={'auto'}
          alt="Scroll down"
        />

        <motion.span>scroll</motion.span>
      </motion.div>
    </Html>
  )
}

export default ScrollDown
