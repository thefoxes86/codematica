// Import Swiper React components
import { Swiper, SwiperSlide, useSwiper } from 'swiper/react'
// Import Swiper styles
import 'swiper/css'
import 'swiper/css/pagination'
import { EffectFade, Mousewheel, Pagination } from 'swiper/modules'
import { useRef } from 'react'
import demoImage from '../../public/images/demo.jpeg'
import { useFrame } from '@react-three/fiber'
import { useScroll } from '@react-three/drei'
import { useState } from 'react'
// Import Swiper styles
import 'swiper/css'
import 'swiper/css/effect-fade'
import Button from './Button'
import Image from 'next/image'
import { services } from '@/utils/mock'
import useBearStore from '@/context/zustand'
import { gsap } from 'gsap'
import TitleSection from './TitleSection'
import { useMediaQuery } from '../hooks/useMediaQuery'
import AnimateSection from './AnimateSection'

const DigitalWinService = ({
  topMd,
  topLg,
  topXl,
  position,
  pages,
  svg,
  servizio,
}) => {
  const isMd = useMediaQuery('(max-width: 768px)')
  const isLg = useMediaQuery('(max-width: 1024px)')
  const isXl = useMediaQuery('(min-width: 1025px)')
  const ref = useRef()
  const swiper = useSwiper()

  const data = useScroll()
  const [topAbs, setTopAbs] = useState(0)
  const [setServiceListProps] = useBearStore(state => [
    state.setServiceListProps,
  ])

  useFrame(() => {
    setTopAbs(data.range(position / pages, position / pages))
  })
  const lastSlideCallback = swiper => {
    if (swiper.isEnd && isXl) {
      //   ref.current.style.pointerEvents = 'none'
      swiper.mousewheel.disable()
      setTimeout(() => {
        swiper.slideTo(0)
        swiper.mousewheel.enable()
      }, 8000)
    }
  }
  const scrollControlls = document.getElementById('scroll-html')

  const centerScreen = e => {
    gsap.to(scrollControlls, {
      transform: `translate3d(0px, -${ref.current.offsetTop - 100}px, 0px)`,
      duration: 1,
      ease: 'power4.out',
    })
  }
  return (
    <div
      className="internal_digitaltwin__section"
      style={{ top: isMd ? topMd : isLg ? topLg : topXl }}
    >
      <AnimateSection
        initial={{ opacity: 0, y: 20 }}
        whileInView={{ opacity: 1, y: 0 }}
        transition={{ duration: 0.9, delay: 0 }}
        viewport={{ amount: 0.3 }}
        className="internal_digitaltwin__item"
      >
        {/* <span className="number">{servizo.number}</span> */}
        <div className="content">
          <div
            className="description"
            dangerouslySetInnerHTML={{ __html: servizio?.content }}
          ></div>
          <Image
            src={servizio.featuredImage.node.sourceUrl}
            width={1000}
            height={300}
            alt=""
          />
        </div>
      </AnimateSection>

      {svg && (
        <Image
          src={svg}
          alt=""
          style={{ transform: `translateY(-${topAbs * 450}px)` }}
        />
      )}
    </div>
  )
}

export default DigitalWinService
