import Image from 'next/image'
import * as THREE from 'three'
import { useRef, useState, useMemo, useEffect } from 'react'
import { useFrame, useThree } from '@react-three/fiber'
import wordsList from '@/utils/words'
import { Text, useScroll } from '@react-three/drei'
import { gsap } from 'gsap'
import { useMediaQuery } from '@/hooks/useMediaQuery'

function Word({ children, ...props }) {
  let finishAnimation = false
  const fontProps = {
    fontSize: 0.55,
    letterSpacing: -0.05,
    lineHeight: 1,
    'material-toneMapped': false,
  }
  const ref = useRef()
  const [hovered, setHovered] = useState(false)
  const over = e => (e.stopPropagation(), setHovered(true))
  const out = () => setHovered(false)
  const data = useScroll()
  // Change the mouse cursor on hover
  useEffect(() => {
    ref.current.color = `#99000000`
    gsap.fromTo(
      ref.current.position,
      { z: -1000 },
      {
        z: props.position.z,
        duration: 2,
        delay: props.index * 0.03,
        ease: 'power2.out',
        onComplete: () => {
          finishAnimation = true
        },
      }
    )
    if (hovered) document.body.style.cursor = 'pointer'
    return () => (document.body.style.cursor = 'auto')
  }, [hovered])
  // Tie component to the render-loop
  useFrame(({ camera, clock }) => {
    // Make text face the camera
    ref.current.quaternion.copy(camera.quaternion)
    // Animate font color

    if (finishAnimation) {
      ref.current.position.z += Math.sin(clock.elapsedTime * 0.5) / 2 / 30

      // ref.current.position.x +=
      //   (Math.sin(clock.elapsedTime * 0.5) / 2 / 50) *
      //   (props.index / Math.floor(8 + Math.random() * (12 - 8)))
      gsap.to(ref.current.position, {
        x: `+=${Math.sin(clock.elapsedTime * 0.5) / 2 / 10}`,
        delay: props.index * 0.2,
        duration: 1.5,
        ease: 'power2.out',
      })

      gsap.to(ref.current.position, {
        y: `+=${Math.sin(clock.elapsedTime * 0.5) / 2 / 20}`,
        delay: props.index * 0.2,
        duration: 1.5,
        ease: 'power2.out',
      })
    }

    const a = data.range(0, 1 / 3)

    if (a >= 0.6) {
      gsap.to(ref.current.position, {
        x: props.index % 2 ? 20 : -20,
        delay: props.index * 0.05,
        duration: 0.5,
        ease: 'power4.easeOut',
      })
    } else {
      gsap.to(ref.current.position, {
        x: props.position.x,
        delay: props.index * 0.05,
        duration: 1.5,
        ease: 'power4.easeIn',
      })
    }
  })
  return (
    <Text
      ref={ref}
      onClick={() => console.log('clicked')}
      {...props}
      {...fontProps}
      children={children}
    />
  )
}

const Cloud = ({ count = 4, radius = 20 }) => {
  // Create a count x count random words with spherical distribution
  const mousePosition = useRef([0, 0])
  const refImage = useRef()
  const { camera } = useThree()
  const material = new THREE.Material()
  const isXs = useMediaQuery('(max-width: 768px)')
  const backgroundImage = new THREE.TextureLoader().load(
    'https://backend.codematica.it/wp-content/uploads/2024/05/shutterstock_1483438988.jpg'
  )

  const words = useMemo(() => {
    const temp = []
    for (let i = 1; i < wordsList.length + 1; i++)
      temp.push([
        new THREE.Vector3(
          Math.random() * 14,
          Math.random() * 6,
          Math.random() * 7
        ),
        wordsList[i - 1],
      ])

    return temp
  }, [count, radius])

  const mouseMovement = e => {
    mousePosition.current = [e.clientX * 3, e.clientY * 3]
  }

  useEffect(() => {
    document.addEventListener('mousemove', mouseMovement)
    console.log('refImage', refImage.current.position)
    gsap.fromTo(
      refImage.current.position,
      {
        z: -100,
      },
      {
        z: 1,
        duration: 6,
        ease: 'power2.out',
      }
    )

    material.visible = false
    return () => document.removeEventListener('mousemove', mouseMovement)
  }, [])

  useFrame(() => {
    gsap.to(camera.position, {
      x: mousePosition.current[0] / 1000,
      y: mousePosition.current[1] / 1000,
      duration: 4.5,
      ease: 'power2.out',
    })
  })

  return (
    <>
      <mesh position={[-8, -3, isXs ? -1 : 1]}>
        <planeGeometry args={[1.9, 1]} />

        <meshBasicMaterial
          attach="material"
          color="black"
          opacity={0}
          transparent
        />
        {words.map(([pos, word], index) => (
          <Word key={index} position={pos} index={index} children={word} />
        ))}
      </mesh>
      <mesh position={[0, 0, isXs ? -1 : 1]} ref={refImage}>
        <planeGeometry args={[3.8 * 3, 2 * 3]} />

        <meshBasicMaterial
          attach="material"
          map={backgroundImage}
          transparent
        />
      </mesh>
    </>
  )
}

export default Cloud
