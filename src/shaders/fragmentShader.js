const fragmentShader = `
varying float vDistance;
uniform vec3 uColorPrimary;
uniform vec3 uColorSecondary;
uniform float uDensity;

void main() {
  vec3 color = vec3(uColorSecondary);
  vec3 colorSec = vec3(uColorPrimary);
  float strength = distance(gl_PointCoord, vec2(0.5));
  strength = 1.0 - strength;
  strength = pow(strength, uDensity);

  color = mix(color, uColorSecondary, vDistance * 1.9);
  color = mix(vec3(0.0), color, strength);
  gl_FragColor = vec4(color, strength);
}
`

export default fragmentShader
