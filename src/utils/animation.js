const animationProps = {
  ease: [0.77, 0, 0.175, 1],
}

const anim = (variants, custom) => {
  return {
    initial: 'initial',
    animate: 'animate',
    exit: 'exit',
    custom,
    variants,
  }
}

export { animationProps, anim }
