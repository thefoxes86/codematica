import { motion } from 'framer-motion'
import { animationProps } from '@/utils/animation'

const LineAnimation = () => {
  const variantsChild = {
    inital: {
      left: '-100vw',
    },
    animate: {
      left: 0,
      transition: {
        duration: 2,
        ease: animationProps.ease,
      },
    },
    exit: {
      left: '100vh',
      transition: {
        duration: 0.5,
        ease: animationProps.ease,
      },
    },
  }
  return (
    <>
      <motion.span
        variants={variantsChild}
        className="h-1/3 w-screen -left-[100vw] bg-black fixed top-0 z-50"
      ></motion.span>
      <motion.span
        variants={variantsChild}
        className="h-1/3 w-screen -left-[100vw] bg-black fixed top-1/3 z-50"
      ></motion.span>
      <motion.span
        variants={variantsChild}
        className="h-1/3 w-screen -left-[100vw] bg-black fixed top-2/3 z-50"
      ></motion.span>
    </>
  )
}

export default LineAnimation
