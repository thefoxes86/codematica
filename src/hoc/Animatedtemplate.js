'use client'

import React from 'react'
import { motion, AnimatePresence } from 'framer-motion'
import { usePathname } from 'next/navigation'
import { LayoutRouterContext } from 'next/dist/shared/lib/app-router-context.shared-runtime'
import { useContext, useRef } from 'react'
import { anim, animationProps } from '@/utils/animation'
import LineAnimation from './LineAnimation'
import Image from 'next/image'

const variants = {
  inital: {
    height: '0',
  },
  animate: {
    height: '100vh',
    transition: {
      duration: 1,
      ease: animationProps.ease,
    },
  },
  exit: {
    top: '100vh',
    transition: {
      duration: 0.5,
      ease: animationProps.ease,
    },
  },
}

// Prevents instant page opening
function FrozenRouter(props) {
  const context = useContext(LayoutRouterContext ?? {})
  const frozen = useRef(context).current

  return (
    <LayoutRouterContext.Provider value={frozen}>
      {props.children}
    </LayoutRouterContext.Provider>
  )
}

export default function AnimatedTemplate({ children }) {
  let pathname = usePathname()
  const frozen = useContext(LayoutRouterContext)

  const nmbOfColumns = 5

  const variantsOpacity = {
    initial: {
      opacity: 0.5,
    },
    animate: {
      opacity: 0,
    },
    exit: {
      opacity: 0.5,
    },
  }
  const variants = {
    initial: {
      top: '0',
    },
    animate: i => ({
      top: '100vh',
      transition: {
        delay: i * 0.05,
        duration: 0.4,
        ease: animationProps.ease,
      },
      transitionEnd: {
        top: '0',
        height: '0',
      },
    }),
    exit: i => ({
      height: '100vh',
      transition: {
        delay: i * 0.05,
        duration: 0.4,
        ease: animationProps.ease,
      },
    }),
  }

  const variantsTransform = {
    initial: {
      y: -20,
    },
    animate: {
      y: 0,
      transition: {
        duration: 1,
        ease: animationProps.ease,
      },
    },
    exit: {
      y: 30,
      transition: {
        duration: 1,
        ease: animationProps.ease,
      },
    },
  }

  return (
    <>
      <AnimatePresence mode={'wait'}>
        <motion.div key={pathname} variants={variants}>
          <FrozenRouter>
            <motion.div
              key={`${pathname}-bg-opacity`}
              {...anim(variantsOpacity)}
              className="h-screen w-screen fixed top-0 left-0 bg-black z-40 pointer-events-none "
            />
            <motion.div
              key={`${pathname}-bg-container`}
              className="flex h-screen w-screen left-0 top-0 fixed z-40 pointer-events-none"
            >
              {[...Array(nmbOfColumns)].map((_, i) => (
                <motion.div
                  key={`${pathname}-${i}-bg-container`}
                  {...anim(variants, nmbOfColumns - i)}
                  className="w-full h-full relative bg-black"
                />
              ))}
            </motion.div>
            <motion.div {...anim(variantsTransform)}>{children}</motion.div>
          </FrozenRouter>
        </motion.div>
      </AnimatePresence>
    </>
  )
}
