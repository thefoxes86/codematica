import { ScrollControls, Scroll } from '@react-three/drei'
import Pages from '@/components/Pages'
import Services from '../components/Services'
import SingleService from '../components/SingleService'
import IconSection from '../components/IconSection'
import IndustrySection from '../components/IndustrySection'
import WorldSection from '../components/WorldSection'
import GreenSection from '../components/GreenSection'
import Footer from '../components/Footer'
import BrandCarousel from '../components/BrandCarousel'
import world from '../../public/images/world.svg'
import infografica from '../../public/images/INFOGRAFICA.png'
import mano from '../../public/images/mano.svg'
import onde from '../../public/images/onde.svg'
import { useRef } from 'react'
import { useFrame, useThree } from '@react-three/fiber'
import { gsap } from 'gsap'
import { useMediaQuery } from '@/hooks/useMediaQuery'
import ServicesInternal from '@/components/ServicesInternal'
const types = [
  { section: 'cloud', position: 0 },
  { section: 'services', position: 1 },
  { section: 'services_list', position: 2 },
  { section: 'icon', position: 3 },
  { section: 'industry', position: 4 },
  {
    section: 'world',
    position: 5,
  },
  {
    section: 'green',
    position: 6,
  },
]

const LayoutHome = ({ data, lang }) => {
  const isXl = useMediaQuery('(min-width: 1025px)')
  const isLg = useMediaQuery('(max-width: 1024px)')
  const isMd = useMediaQuery('(max-width: 768px)')
  const pages = isMd ? 12.7 : isLg ? 6.3 : 8.0
  const ref = useRef()

  return (
    <>
      <ScrollControls pages={pages}>
        <Scroll>
          <Pages type={types} />
        </Scroll>
        <Scroll html id="scroll-html">
          {isXl ? (
            <Services
              servizi={data.servizi}
              topMd={'100vh'}
              topLg={'100vh'}
              topXl={'100vh'}
              position={0}
              pages={pages}
              svg={null}
              lang={lang}
            />
          ) : null}
          {isLg ? (
            <ServicesInternal
              topMd={'100vh'}
              topLg={'100vh'}
              topXl={'200vh'}
              position={1}
              pages={pages}
              svg={null}
              servizi={data?.servizi}
              lang={lang}
            />
          ) : (
            <SingleService
              servizi={data.servizi}
              topMd={'200vh'}
              topLg={'200vh'}
              topXl={'200vh'}
              position={1}
              pages={pages}
              svg={null}
              lang={lang}
            />
          )}
          <IconSection
            topMd={'870vh'}
            topLg={'260vh'}
            topXl={'300vh'}
            position={2}
            pages={pages}
            svg={infografica}
          />
          <IndustrySection
            topMd={'900vh'}
            topLg={'340vh'}
            topXl={'400vh'}
            position={3}
            pages={pages}
            svg={mano}
          />
          <WorldSection
            topMd={'1000vh'}
            topLg={'400vh'}
            topXl={'500vh'}
            position={4}
            pages={pages}
            svg={world}
          />
          <BrandCarousel
            brands={data.brands}
            topMd={'1060vh'}
            topLg={'450vh'}
            topXl={'600vh'}
            position={5}
            pages={pages}
            svg={null}
          />
          <GreenSection
            topMd={'1100vh'}
            topLg={'490vh'}
            topXl={'650vh'}
            position={6}
            pages={pages}
            svg={onde}
          />
          <Footer
            topMd={'1170vh'}
            topLg={'540vh'}
            topXl={'740vh'}
            position={7}
          />
        </Scroll>
      </ScrollControls>
    </>
  )
}

export default LayoutHome
