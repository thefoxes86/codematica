import { ScrollControls, Scroll } from '@react-three/drei'
import industriainfo from '../../public/images/industriainfo.png'
import Pages from '@/components/Pages'
import world from '../../public/images/world.svg'
import ondeTop from '../../public/images/ondeTop.svg'

import Footer from '../components/Footer'
import IconSection from '@/components/IconSection'
import WorldSection from '@/components/WorldSection'
import TitleSection from '@/components/TitleSection'
import RealtaAumtataSection from '@/components/RealtaAumtataSection'
import { useMediaQuery } from '@/hooks/useMediaQuery'

const types = [
  { section: 'icon', position: 0 },
  { section: 'industry', position: 1 },
]
const LayoutIndustria = () => {
  const isMd = useMediaQuery('(max-width: 768px)')
  const pages = isMd ? 3.4 : 3.4
  return (
    <>
      <ScrollControls pages={pages}>
        <Scroll>
          <Pages type={types} />
        </Scroll>
        <Scroll html>
          <TitleSection
            top={'0'}
            title={'INDUSTRIA 5.0'}
            svg={ondeTop}
            pages={pages}
            position={0.5}
          />
          <IconSection
            topMd={'10vh'}
            topLg={'30vh'}
            topXl={'30vh'}
            position={1}
            pages={pages}
            svg={industriainfo}
          />
          <RealtaAumtataSection
            topMd={'100vh'}
            topLg={'120vh'}
            topXl={'120vh'}
            position={1.5}
            pages={pages}
          />
          <WorldSection
            topMd={'160vh'}
            topLg={'180vh'}
            topXl={'180vh'}
            position={1.5}
            pages={pages}
            svg={world}
          />

          <Footer
            topMd={'240vh'}
            topLg={'280vh'}
            topXl={'280vh'}
            position={2}
          />
        </Scroll>
      </ScrollControls>
    </>
  )
}

export default LayoutIndustria
