import { ScrollControls, Scroll } from '@react-three/drei'
import test from '../../public/images/test.svg'
import Pages from '@/components/Pages'
import IndustrySection from '../components/IndustrySection'

import Footer from '../components/Footer'

import ServicesInternal from '@/components/ServicesInternal'
import TitleSection from '@/components/TitleSection'
import SingleService from '@/components/SingleService'
import GreenSection from '@/components/GreenSection'
import CertificazioniList from '@/components/CertificazioniList'
import Scrivici from '@/components/Scrivici'
import CertificazioniSection from '@/components/CertificazioniSection'
import ondeTop from '../../public/images/ondeTop.svg'
import Form from '@/components/Form'
import world from '../../public/images/world.svg'
import { useMediaQuery } from '@/hooks/useMediaQuery'
import FormCondidati from '@/components/FormCondidati'

const types = [
  { section: '', position: 0 },
  { section: 'certificazioni', position: 1 },
]
const LayoutLavoraConNoi = () => {
  const isMd = useMediaQuery('(max-width: 768px)')
  const pages = isMd ? 2.4 : 2.1
  return (
    <>
      <ScrollControls pages={pages}>
        <Scroll>
          <Pages type={types} />
        </Scroll>
        <Scroll html>
          {/* <Services />
          <SingleService /> */}
          <TitleSection
            top={'0'}
            title={'LAVORA CON NOI'}
            svg={ondeTop}
            pages={pages}
            position={0.5}
          />
          <FormCondidati
            topMd={'30vh'}
            topLg={'40vh'}
            topXl={'60vh'}
            position={1}
            pages={pages}
            svg={world}
          />

          <Footer
            topMd={'140vh'}
            topLg={'150vh'}
            topXl={'150vh'}
            position={4}
          />
        </Scroll>
      </ScrollControls>
    </>
  )
}

export default LayoutLavoraConNoi
