import { ScrollControls, Scroll } from '@react-three/drei'
import test from '../../public/images/test.svg'
import Pages from '@/components/Pages'
import IndustrySection from '../components/IndustrySection'

import Footer from '../components/Footer'

import ServicesInternal from '@/components/ServicesInternal'
import TitleSection from '@/components/TitleSection'
import SingleService from '@/components/SingleService'
import mano from '../../public/images/mano.svg'
import { useMediaQuery } from '@/hooks/useMediaQuery'
import ondeTop from '../../public/images/ondeTop.svg'
import DigitalWinService from '@/components/DigitalWinService'

const types = [
  { section: 'services_internal', position: 0 },
  { section: 'world', position: 1 },
  { section: 'icon', position: 2 },
  { section: 'industry', position: 3 },
]
const LayoutDigitalWin = ({ data }) => {
  const isLg = useMediaQuery('(max-width: 1024px)')
  const isMd = useMediaQuery('(max-width: 768px)')
  const pages = isMd ? 3.7 : 2.7
  return (
    <>
      <ScrollControls pages={pages}>
        <Scroll>
          <Pages type={types} />
        </Scroll>
        <Scroll html>
          {/* <Services />
          <SingleService /> */}
          <TitleSection
            top={`${isMd ? '0' : '-5vh'}`}
            title={'DIGITAL TWIN'}
            svg={ondeTop}
            pages={pages}
            position={0.5}
          />

          <DigitalWinService
            topMd={'30vh'}
            topLg={'20vh'}
            topXl={'20vh'}
            position={1}
            pages={pages}
            svg={null}
            servizio={data?.servizio}
          />

          <IndustrySection
            topMd={'190vh'}
            topLg={'140vh'}
            topXl={'120vh'}
            position={3}
            pages={pages}
            svg={mano}
          />
          <Footer
            topMd={'270vh'}
            topLg={'220vh'}
            topXl={'210vh'}
            position={4}
          />
        </Scroll>
      </ScrollControls>
    </>
  )
}

export default LayoutDigitalWin
