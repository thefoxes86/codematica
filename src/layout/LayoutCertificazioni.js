import { ScrollControls, Scroll } from '@react-three/drei'
import test from '../../public/images/test.svg'
import Pages from '@/components/Pages'
import IndustrySection from '../components/IndustrySection'

import Footer from '../components/Footer'

import ServicesInternal from '@/components/ServicesInternal'
import TitleSection from '@/components/TitleSection'
import SingleService from '@/components/SingleService'
import GreenSection from '@/components/GreenSection'
import CertificazioniList from '@/components/CertificazioniList'
import Scrivici from '@/components/Scrivici'
import onde from '../../public/images/onde.svg'
import ondeTop from '../../public/images/ondeTop.svg'
import { useMediaQuery } from '@/hooks/useMediaQuery'

const types = [
  { section: 'services_internal', position: 0 },
  { section: 'green', position: 1 },
  { section: 'scrivici', position: 2 },
]
const LayoutCertificazioni = ({data}) => {
  const isMd = useMediaQuery('(max-width: 768px)')
  const pages = isMd ? 4.1 : 3.2
  return (
    <>
      <ScrollControls pages={pages}>
        <Scroll>
          <Pages type={types} />
        </Scroll>
        <Scroll html>
          {/* <Services />
          <SingleService /> */}
          <TitleSection
            top={'0'}
            title={'CERTIFICAZIONI'}
            svg={ondeTop}
            pages={pages}
            position={0.5}
          />
          <CertificazioniList
            topMd={'30vh'}
            topLg={'40vh'}
            topXl={'40vh'}
            position={1}
            pages={pages}
            svg={null}
            contents={data?.page?.contenuti}
          />
          <GreenSection
            topMd={'160vh'}
            topLg={'100vh'}
            topXl={'100vh'}
            position={2}
            pages={pages}
            svg={onde}
          />

          <Scrivici
            topMd={'240vh'}
            topLg={'160vh'}
            topXl={'190vh'}
            position={3}
          />
          <Footer
            topMd={'310vh'}
            topLg={'210vh'}
            topXl={'260vh'}
            position={4}
          />
        </Scroll>
      </ScrollControls>
    </>
  )
}

export default LayoutCertificazioni
