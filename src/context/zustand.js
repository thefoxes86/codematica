import { create } from 'zustand'

const useBearStore = create(set => ({
  logoLight: true,
  cycleLogoLight: () =>
    set(state => ({ logoLight: (state.logoLight = !state.logoLight) })),
  serviceListProps: { top: 0 },
  setServiceListProps: value =>
    set(state => ({ serviceListprops: (state.serviceListprops = value) })),
}))

export default useBearStore
