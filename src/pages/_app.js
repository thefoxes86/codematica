import CookieConsentBanner from '@/components/CookieConsentBanner'
import '@/styles/globals.css'
import { AnimatePresence, motion } from 'framer-motion'

const variants = {
  initialState: {
    transform: 'scale(1.1)',
    opacity: 0,
  },
  animateState: {
    transform: 'scale(1)',
    opacity: 1,
  },
  exitState: {
    transform: 'scale(0.9)',
    opacity: 0,
  },
}

export default function App({ Component, pageProps }) {
  return (
    <AnimatePresence mode="wait">
      <motion.div
        key={Math.random() * 100}
        initial="initialState"
        animate="animateState"
        className="mx-auto page-content"
        exit="exitState"
        variants={variants}
        transition={{
          ease: 'easeInOut',
          duration: 1,
        }}
        id="app"
      >
        <Component {...pageProps} />
      </motion.div>
    </AnimatePresence>
  )
}
