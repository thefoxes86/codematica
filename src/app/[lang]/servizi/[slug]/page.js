import ServiceItem from '@/components/ServiceItem'

export async function generateStaticParams() {
  const servizi = [
    'digital-twin-realizzazione-di-impianti-virtuali',
    'progettazione-software',
    'ricerca-e-sviluppo',
    'ingegneria-elettrica',
    'realizzazione-quadri-elettricibt-e-mt',
    'progettazione-soluzioni-diottimizzazioni-diprocesso',
    'progettazionemeccanica-di-parti-dimacchina-o-soluzioniinnovative',
    'cerificati',
    'integrazione-disistemi-tecnologicivisione',
    'progettazionescada-supervisionie-bms',
    'sistemi-raccoltadata-analyst',
    'revampingmanutenzione-eassistenza',
  ]

  return servizi.map(node => ({
    slug: node,
  }))
}

export default function Page({ params }) {
  return <ServiceItem {...params} />
}
