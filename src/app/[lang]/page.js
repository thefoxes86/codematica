'use client'

import Head from 'next/head'
import { Inter } from '@next/font/google'
import styles from '@/styles/Home.module.css'
import { Canvas } from '@react-three/fiber'
import { Fragment, Suspense, useEffect, useRef } from 'react'
import CameraScroll from '@/components/CameraScroll'
import { gsap } from 'gsap'
import { CAMERA_DIST } from '@/utils/variables'
import { OrbitControls, PerspectiveCamera } from '@react-three/drei'
import Header from '@/components/Header'
import LayoutHome from '@/layout/LayoutHome'
import { useSuspenseQuery } from '@apollo/client'
import { GET_HOME } from '@/graphql/query'

const inter = Inter({ subsets: ['latin'] })

export default function Page({ params }) {
  const { data, error } = useSuspenseQuery(GET_HOME, {
    variables: { language: params.lang?.toUpperCase() },
  })
  const ref = useRef()
  const resolution = useRef()
  const fov = useRef()
  const containerText = useRef()

  useEffect(() => {
    fov.current =
      2 * Math.atan(window.innerHeight / (2 * CAMERA_DIST)) * (180 / Math.PI)
    resolution.current = window.innerWidth / window.innerHeight
    gsap.to(ref.current, {
      x: '+=40',
      ease: 'none',
      scrollTrigger: {
        trigger: '.scroll--right',
        start: 'top top',
        end: 'bottom bottom',
        scrub: true,
      },
    })
  }, [])
  return (
    <Suspense fallback={'loading'}>
      <div className="home">
        <main className={styles.main} id="main-section">
          <Header lang={params.lang} />
          <Canvas dpr={[1, 2]}>
            <PerspectiveCamera
              makeDefault
              resolution={resolution.current}
              fov={fov.current}
              near={0.001}
              far={1000}
              position={[0, 0, 10]}
            />

            <ambientLight />
            <Suspense fallback={'loading'}>
              <LayoutHome data={data} lang={params.lang} />
              <CameraScroll />
            </Suspense>
          </Canvas>
        </main>
      </div>
    </Suspense>
  )
}
