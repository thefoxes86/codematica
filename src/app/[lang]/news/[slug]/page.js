'use client'
import { Inter } from '@next/font/google'
import styles from '@/styles/Home.module.css'

import { Suspense } from 'react'

import Header from '@/components/Header'

import { useSuspenseQuery } from '@apollo/client'
import { GET_NEWS, GET_NEWS_DETAIL, PAGE_SINGLE_QUERY } from '@/graphql/query'
import Link from 'next/link'
import Footer from '@/components/Footer'

const inter = Inter({ subsets: ['latin'] })

export default function Page({ params }) {
  console.log('Params', params)
  const { data } = useSuspenseQuery(GET_NEWS_DETAIL, {
    variables: {
      id: params.slug,
      language: params.lang?.toUpperCase(),
      lang: params.lang?.toUpperCase(),
    },
  })

  return (
    <Suspense fallback={'loading'}>
      <div className="news_page">
        <main className={styles.main}>
          <Header lang={params.lang} />
          <div className="container_no_three">
            <div className="single_post">
              <h1 dangerouslySetInnerHTML={{ __html: data.post.title }}></h1>
              <slug>{`${new Date(data.post.date).getDate()}-${
                new Date(data.post.date).getMonth() + 1
              }-${new Date(data.post.date).getFullYear()}`}</slug>

              <img
                src={data.post.featuredImage?.node?.sourceUrl || 'fallback'}
              />
              <p dangerouslySetInnerHTML={{ __html: data.post.content }}></p>
              <Link className="read_all btn" href={`/news/`}>
                aLL NEWS
              </Link>
            </div>
          </div>
          <Footer absolute={false} />
        </main>
      </div>
    </Suspense>
  )
}
