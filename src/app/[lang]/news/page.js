'use client'
import { Inter } from '@next/font/google'
import styles from '@/styles/Home.module.css'

import { Suspense } from 'react'

import Header from '@/components/Header'

import { useSuspenseQuery } from '@apollo/client'
import { GET_NEWS, PAGE_SINGLE_QUERY } from '@/graphql/query'
import Link from 'next/link'
import Footer from '@/components/Footer'

const inter = Inter({ subsets: ['latin'] })

export default function Page({ params }) {
  const { data } = useSuspenseQuery(GET_NEWS, {
    variables: {
      language: params.lang?.toUpperCase(),
      lang: params.lang?.toUpperCase(),
    },
  })

  return (
    <Suspense fallback={'loading'}>
      <div className="news_page">
        <main className={styles.main}>
          <Header lang={params.lang} />
          <div className="container_no_three">
            <h1>News</h1>

            <div className="content_posts">
              {data.posts.nodes.length > 0 ? (
                data.posts.nodes.map(post => (
                  <div className="post">
                    <Link href={`/news/${post.slug}`}>
                      <img
                        src={post.featuredImage?.node?.sourceUrl || 'fallback'}
                      />
                    </Link>
                    <slug>{`${new Date(post.date).getDate()}-${
                      new Date(post.date).getMonth() + 1
                    }-${new Date(post.date).getFullYear()}`}</slug>
                    <h3 dangerouslySetInnerHTML={{ __html: post.title }}></h3>
                    <Link className="read_all btn" href={`/news/${post.slug}`}>
                      Read All
                    </Link>
                  </div>
                ))
              ) : (
                <h2 className="no_posts">
                  Ci scusiamo ma al momento non ci sono news
                </h2>
              )}
            </div>
          </div>
          <Footer absolute={false} />
        </main>
      </div>
    </Suspense>
  )
}
