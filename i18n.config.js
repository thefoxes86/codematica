const originalLanguages = ['it', 'es', 'en', 'ar']
export const i18n = {
  defaultLocale: 'it',
  locales: ['it', 'en'],
}
