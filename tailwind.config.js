/** @type {import('tailwindcss').Config} */
module.exports = {
  purge: ['./src/**/*.{js,ts,jsx,tsx}'],
  darkMode: false, // or 'media' or 'class'
  theme: {
    extend: {
      lineHeight: {
        xl: '140px',
      },
      fontSize: {
        titleXl: '140px',
        titleLg: '100px',
        titleMd: '60px',
        titleSm: '40px',
        titleXs: '20px',
      },
      height: {
        footerNormal: '20vh',
        footerXl: '30vh',
        footerLg: '40vh',
        footerMd: '70vh',
      },
      width: {
        swiperLg: '1024px',
        swiperMd: '768px',
        swiperXl: '100vw',
      },
    },
  },
  plugins: [],
}
